<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine;

/**
 * Class ChainNamespaceGetter
 *
 * @author Benjamin Georgeault
 */
class ChainNamespaceGetter implements NamespaceGetterInterface
{
    /**
     * @var NamespaceGetterInterface[]
     */
    private $getters;

    /**
     * ChainNamespaceGetter constructor.
     * @param NamespaceGetterInterface[] $getters
     */
    public function __construct(array $getters = [])
    {
        $this->getters = [];

        foreach ($getters as $getter) {
            $this->addNamespaceGetter($getter);
        }
    }

    /**
     * @inheritDoc
     */
    public function getNamespace(string $class): ?string
    {
        foreach ($this->getters as $getter) {
            if (null !== $namespace = $getter->getNamespace($class)) {
                return $namespace;
            }
        }

        return null;
    }

    /**
     * @param NamespaceGetterInterface $namespaceGetter
     * @return ChainNamespaceGetter
     */
    public function addNamespaceGetter(NamespaceGetterInterface $namespaceGetter): ChainNamespaceGetter
    {
        if (!in_array($namespaceGetter, $this->getters)) {
            $this->getters[] = $namespaceGetter;
        }

        return $this;
    }
}

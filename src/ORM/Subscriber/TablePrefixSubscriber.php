<?php

/*
 * This file is part of the pressop/doctrine package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;

/**
 * Use to prefix tables' name for a specific Entity namespace.
 *
 * @author Benjamin Georgeault
 */
class TablePrefixSubscriber implements EventSubscriber
{
    /**
     * @var string
     */
    private $prefix;

    /**
     * @var string
     */
    private $namespace;

    /**
     * TablePrefixSubscriber constructor.
     * @param string $prefix
     * @param string $namespace
     */
    public function __construct(string $prefix, string $namespace)
    {
        $this->prefix = $prefix;
        $this->namespace = $namespace;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        if ($metadata->isInheritanceTypeSingleTable() && !$metadata->isRootEntity()) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();

        if (false !== strstr($reflectionClass->getNamespaceName(), $this->namespace)) {
            $metadata->setPrimaryTable([
                'name' => $this->prefix . $metadata->getTableName(),
            ]);

            foreach ($metadata->getAssociationMappings() as $fieldName => $mapping) {
                if (
                    $mapping['type'] == \Doctrine\ORM\Mapping\ClassMetadataInfo::MANY_TO_MANY &&
                    array_key_exists('name', $metadata->associationMappings[$fieldName]['joinTable'])
                ) {
                    $metadata->associationMappings[$fieldName]['joinTable']['name'] =
                        $this->prefix . $metadata->associationMappings[$fieldName]['joinTable']['name'];
                }
            }
        }
    }
}

<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine\Bridge\Symfony\Form\Type;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Pressop\Component\Doctrine\ModelTranslationInterface;
use Pressop\Component\Doctrine\NamespaceGetterInterface;
use Pressop\Component\Inflector\Inflector;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ModelType
 *
 * @author Benjamin Georgeault
 */
class ModelType extends AbstractType
{
    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * @var NamespaceGetterInterface
     */
    private $namespaceGetter;

    /**
     * @var ModelTranslationInterface|null
     */
    private $modelTranslation;

    /**
     * ModelType constructor.
     * @param ManagerRegistry $registry
     * @param NamespaceGetterInterface $namespaceGetter
     * @param null|ModelTranslationInterface $modelTranslation
     */
    public function __construct(
        ManagerRegistry $registry,
        NamespaceGetterInterface $namespaceGetter,
        ModelTranslationInterface $modelTranslation = null
    ) {
        $this->registry = $registry;
        $this->namespaceGetter = $namespaceGetter;
        $this->modelTranslation = $modelTranslation;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (null === $manager = $this->registry->getManagerForClass($options['model_class'])) {
            throw new \LogicException(sprintf('Missing Manager for "%s".', $options['model_class']));
        }

        /** @var ClassMetadata $metadata */
        $metadata = $manager->getClassMetadata($options['model_class']);

        foreach ($metadata->getFieldNames() as $fieldName) {
            if (
                in_array($fieldName, $metadata->getIdentifierFieldNames()) ||
                preg_match('/Canonical$/', $fieldName) ||
                preg_match('/^(created)|(updated)(At)|(By)/', $fieldName)
            ) {
                continue;
            }

            $builder->add($fieldName, null, $this->getFieldOptions($metadata, $fieldName, $options));
        }
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => function (Options $options) {
                return $options['model_class'];
            },
            'block_name' => function (Options $options) {
                return Inflector::tableize(Inflector::basename($options['model_class']));
            },
            'translation_domain' => function (Options $options) {
                return $this->namespaceGetter->getNamespace($options['model_class']) ? : 'messages';
            },
        ]);

        $resolver->setRequired([
            'model_class',
        ]);

        $resolver->setAllowedTypes('model_class', 'string');
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        return 'pressop_doctrine_model';
    }

    /**
     * @param ClassMetadata $metadata
     * @param string $fieldName
     * @param array $options
     * @return array
     */
    protected function getFieldOptions(ClassMetadata $metadata, string $fieldName, array $options): array
    {
        $fieldOptions = [];

        if (null !== $this->modelTranslation) {
            $label = $this->modelTranslation->getFieldLabel(
                $fieldName,
                $options['data_class'],
                $options['translation_domain']
            );

            if (null !== $label) {
                $fieldOptions = array_merge($fieldOptions, [
                    'label' => $label,
                    'translation_domain' => false,
                ]);
            }
        }

        return $fieldOptions;
    }
}

<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine\Bridge\Symfony\HttpKernel;

use Pressop\Component\Doctrine\NamespaceGetterInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class NamespaceGetter
 *
 * @author Benjamin Georgeault
 */
class NamespaceGetter implements NamespaceGetterInterface
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * DefinitionNamespaceGetter constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @inheritDoc
     */
    public function getNamespace(string $class): ?string
    {
        foreach ($this->kernel->getBundles() as $alias => $bundle) {
            if (preg_match('/^'.preg_quote($bundle->getNamespace()) . '/', $class)) {
                return $alias;
            }
        }

        return null;
    }
}

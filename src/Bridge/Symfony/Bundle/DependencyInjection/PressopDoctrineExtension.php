<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine\Bridge\Symfony\Bundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class PressopDoctrineExtension
 *
 * @author Benjamin Georgeault
 */
class PressopDoctrineExtension extends Extension implements PrependExtensionInterface
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('event_subscribers.yaml');
        $loader->load('namespace.yaml');


        if ($config['bridges']['enabled']) {
            if ($config['bridges']['forms']['enabled']) {
                if (!$config['bridges']['translations']['enabled']) {
                    throw new LogicException(sprintf('Enable "form" require to enable "translations" too.'));
                }

                $loader->load('bridges/forms.yaml');
            }

            if ($config['bridges']['translations']['enabled']) {
                $loader->load('bridges/translations.yaml');
            }

            if ($config['bridges']['twig']['enabled']) {
                $loader->load('bridges/twig.yaml');
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function prepend(ContainerBuilder $container)
    {
        if ($container->hasExtension('framework')) {
            $container->prependExtensionConfig('framework', [
                'translator' => [
                    'paths' => [
                        __DIR__ . '/../../../../../translations',
                    ],
                ],
            ]);
        }
    }
}

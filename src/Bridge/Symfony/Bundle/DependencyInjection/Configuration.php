<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine\Bridge\Symfony\Bundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @author Benjamin Georgeault
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();

        $builder->root('pressop_doctrine')
            ->children()
                ->arrayNode('bridges')
                    ->canBeEnabled()
                    ->children()
                        ->arrayNode('forms')
                            ->canBeDisabled()
                        ->end()
                        ->arrayNode('translations')
                            ->canBeDisabled()
                        ->end()
                        ->arrayNode('twig')
                            ->canBeDisabled()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $builder;
    }
}

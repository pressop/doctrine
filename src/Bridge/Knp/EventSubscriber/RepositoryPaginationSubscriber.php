<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine\Bridge\Knp\EventSubscriber;

use Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\Event\ItemsEvent;
use Pressop\Component\Inflector\Inflector;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class RepositoryPaginationSubscriber
 *
 * @author Benjamin Georgeault
 */
class RepositoryPaginationSubscriber implements EventSubscriberInterface
{
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        if (class_exists('Doctrine\\ORM\\EntityRepository')) {
            return [
                'knp_pager.items' => ['items', 11],
            ];
        }

        return [];
    }

    /**
     * @param ItemsEvent $event
     */
    public function items(ItemsEvent $event)
    {
        if ($event->target instanceof EntityRepository) {
            $event->target = $event->target->createQueryBuilder(Inflector::basename($event->target->getClassName()));
        }
    }
}

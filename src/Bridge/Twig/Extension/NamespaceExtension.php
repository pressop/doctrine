<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine\Bridge\Twig\Extension;

use Pressop\Component\Doctrine\NamespaceGetterInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class NamespaceExtension
 *
 * @author Benjamin Georgeault
 */
class NamespaceExtension extends AbstractExtension
{
    /**
     * @var NamespaceGetterInterface
     */
    private $namespaceGetter;

    /**
     * NamespaceExtension constructor.
     * @param NamespaceGetterInterface $namespaceGetter
     */
    public function __construct(NamespaceGetterInterface $namespaceGetter)
    {
        $this->namespaceGetter = $namespaceGetter;
    }

    /**
     * @inheritDoc
     */
    public function getFilters()
    {
        return [
            new TwigFilter('dnamespace', [$this->namespaceGetter, 'getNamespace']),
        ];
    }
}

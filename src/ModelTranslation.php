<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine;

use Pressop\Component\Inflector\Inflector;
use Pressop\Component\Translation\ChainTranslatorInterface;

/**
 * Class ModelTranslation
 *
 * @author Benjamin Georgeault
 */
class ModelTranslation implements ModelTranslationInterface
{
    /**
     * @var ChainTranslatorInterface
     */
    private $translator;

    /**
     * @var NamespaceGetterInterface
     */
    private $namespaceGetter;

    /**
     * ModelTranslation constructor.
     * @param ChainTranslatorInterface $translator
     * @param NamespaceGetterInterface $namespaceGetter
     */
    public function __construct(ChainTranslatorInterface $translator, NamespaceGetterInterface $namespaceGetter)
    {
        $this->translator = $translator;
        $this->namespaceGetter = $namespaceGetter;
    }

    /**
     * @inheritDoc
     */
    public function getFieldLabel(string $fieldName, string $class = null, string $domain = null): string
    {
        if (null === $domain && null !== $class) {
            $domain = $this->namespaceGetter->getNamespace($class);
        }

        $keys = [
            sprintf('defaults.model.mostUsed.%s.label', $fieldName) => 'PressopDoctrine',
            'defaults.model.defaultField.label' => 'PressopDoctrine',
        ];

        if (null !== $class) {
            array_unshift($keys, sprintf(
                '%s.model.%s.label',
                Inflector::tableize(Inflector::pluralize(Inflector::basename($class))),
                $fieldName
            ));
        }

        $humanizedFieldName = Inflector::humanize($fieldName);

        $label = $this->translator->trans($keys, [
            '%field_name%' => $fieldName,
            '%field_humanize%' => $humanizedFieldName,
        ], $domain, null, false);

        if (null === $label) {
            return $humanizedFieldName;
        }

        return $label;
    }
}

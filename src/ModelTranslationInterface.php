<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Doctrine;

/**
 * Interface ModelTranslationInterface
 *
 * @author Benjamin Georgeault
 */
interface ModelTranslationInterface
{
    /**
     * @param string $fieldName
     * @param string|null $class
     * @param string|null $domain
     * @return string
     */
    public function getFieldLabel(string $fieldName, string $class = null, string $domain = null): string;
}

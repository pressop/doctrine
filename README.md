Doctrine
=============

Some utils stuffs to work with Doctrine2.

## Install

```
composer require pressop/doctrine
```

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
